//
//  CGFloat+Padding.swift
//  Tanuki
//
//  Created by Lorenzo Lewis on 1/12/21.
//

import UIKit

extension CGFloat {
    static let glSpacer8: CGFloat = 8
}
