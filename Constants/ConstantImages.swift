//
//  ConstantImages.swift
//  Tanuki
//
//  Created by Lorenzo Lewis on 1/24/21.
//

import UIKit

extension UIImage {
    static var glLogo: UIImage { UIImage(named: "TanukiLogo")! }
}
