//
//  ConstantUserDefaultsKey.swift
//  Tanuki
//
//  Created by Lorenzo Lewis on 1/4/21.
//

import Foundation

struct Constants {
    enum UserDefaultsKey: String {
        case baseUrl
    }
}
