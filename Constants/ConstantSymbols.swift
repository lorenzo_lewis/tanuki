//
//  ConstantSymbols.swift
//  Tanuki
//
//  Created by Lorenzo Lewis on 12/30/20.
//

import UIKit

extension UIImage {
    static var glBranch: UIImage { UIImage(named: "Branch")! }
    static var glChevronRight: UIImage { UIImage(named: "Chevron right")! }
    static var glChevronDown: UIImage { UIImage(named: "Chevron down")! }
    static var glCommit: UIImage { UIImage(named: "Commit")! }
    static var glDisk: UIImage { UIImage(named: "Disk")! }
    static var glDocCode: UIImage { UIImage(named: "Doc code")! }
    static var glFork: UIImage { UIImage(named: "fork")! }
    static var glHome: UIImage { UIImage(named: "home")! }
    static var glLabel: UIImage { UIImage(named: "Label")! }
    static var glNotification: UIImage { UIImage(named: "notification")! }
    static var glProject: UIImage { UIImage(named: "project")! }
    static var glSearch: UIImage { UIImage(named: "search")! }
    static var glStar: UIImage { UIImage(named: "star")! }
    static var glUser: UIImage { UIImage(named: "user")! }

}
