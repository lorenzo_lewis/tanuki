//
//  ConstantIllustrations.swift
//  Tanuki
//
//  Created by Lorenzo Lewis on 1/24/21.
//

import UIKit

extension UIImage {
    enum Illustrations {
        static var userNotLoggedIn: UIImage { UIImage(named: "user-not-logged-in")! }
    }
}
