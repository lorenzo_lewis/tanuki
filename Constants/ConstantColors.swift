//
//  ConstantColors.swift
//  Tanuki
//
//  Created by Lorenzo Lewis on 12/30/20.
//

import UIKit

extension UIColor {

    // MARK:- Gray
    /// Weight 500
    static var glGray1: UIColor { UIColor(named: "gray1")! }
    /// Weight 400 and 600
    static var glGray2: UIColor { UIColor(named: "gray2")! }
    /// Weight 300 and 700
    static var glGray3: UIColor { UIColor(named: "gray3")! }
    /// Weight 200 and 800
    static var glGray4: UIColor { UIColor(named: "gray4")! }
    /// Weight 100 and 900
    static var glGray5: UIColor { UIColor(named: "gray5")! }
    /// Weight 50 and 950
    static var glGray6: UIColor { UIColor(named: "gray6")! }

    // MARK:- Blue
    /// Weight 500
    static var glBlue1: UIColor { UIColor(named: "blue1")! }
    /// Weight 400 and 600
    static var glBlue2: UIColor { UIColor(named: "blue2")! }
    /// Weight 300 and 700
    static var glBlue3: UIColor { UIColor(named: "blue3")! }
    /// Weight 200 and 800
    static var glBlue4: UIColor { UIColor(named: "blue4")! }
    /// Weight 100 and 900
    static var glBlue5: UIColor { UIColor(named: "blue5")! }
    /// Weight 50 and 950
    static var glBlue6: UIColor { UIColor(named: "blue6")! }

    // MARK:- Green
    /// Weight 500
    static var glGreen1: UIColor { UIColor(named: "green1")! }
    /// Weight 400 and 600
    static var glGreen2: UIColor { UIColor(named: "green2")! }
    /// Weight 300 and 700
    static var glGreen3: UIColor { UIColor(named: "green3")! }
    /// Weight 200 and 800
    static var glGreen4: UIColor { UIColor(named: "green4")! }
    /// Weight 100 and 900
    static var glGreen5: UIColor { UIColor(named: "green5")! }
    /// Weight 50 and 950
    static var glGreen6: UIColor { UIColor(named: "green6")! }

    // MARK:- Orange
    /// Weight 500
    static var glOrange1: UIColor { UIColor(named: "orange1")! }
    /// Weight 400 and 600
    static var glOrange2: UIColor { UIColor(named: "orange2")! }
    /// Weight 300 and 700
    static var glOrange3: UIColor { UIColor(named: "orange3")! }
    /// Weight 200 and 800
    static var glOrange4: UIColor { UIColor(named: "orange4")! }
    /// Weight 100 and 900
    static var glOrange5: UIColor { UIColor(named: "orange5")! }
    /// Weight 50 and 950
    static var glOrange6: UIColor { UIColor(named: "orange6")! }

    // MARK:- Red
    /// Weight 500
    static var glRed1: UIColor { UIColor(named: "red1")! }
    /// Weight 400 and 600
    static var glRed2: UIColor { UIColor(named: "red2")! }
    /// Weight 300 and 700
    static var glRed3: UIColor { UIColor(named: "red3")! }
    /// Weight 200 and 800
    static var glRed4: UIColor { UIColor(named: "red4")! }
    /// Weight 100 and 900
    static var glRed5: UIColor { UIColor(named: "red5")! }
    /// Weight 50 and 950
    static var glRed6: UIColor { UIColor(named: "red6")! }
    
    // MARK:- Purple
    /// Weight 500
    static var glPurple1: UIColor { UIColor(named: "purple1")! }
    /// Weight 400 and 600
    static var glPurple2: UIColor { UIColor(named: "purple2")! }
    /// Weight 300 and 700
    static var glPurple3: UIColor { UIColor(named: "purple3")! }
    /// Weight 200 and 800
    static var glPurple4: UIColor { UIColor(named: "purple4")! }
    /// Weight 100 and 900
    static var glPurple5: UIColor { UIColor(named: "purple5")! }
    /// Weight 50 and 950
    static var glPurple6: UIColor { UIColor(named: "purple6")! }
    
    // MARK:- Theme
    private enum Theme: String { case Indigo }

    private static func getTheme() -> Theme {
        if let theme = UserDefaults.standard.object(forKey: "theme") as? Theme {
            return theme
        }
        return Theme.Indigo
    }

    static var theme1: UIColor { UIColor(named: "\(getTheme())1")! }
    static var theme2: UIColor { UIColor(named: "\(getTheme())2")! }
    static var theme3: UIColor { UIColor(named: "\(getTheme())3")! }
    static var theme4: UIColor { UIColor(named: "\(getTheme())4")! }
    static var theme5: UIColor { UIColor(named: "\(getTheme())5")! }
    static var theme6: UIColor { UIColor(named: "\(getTheme())6")! }
}
