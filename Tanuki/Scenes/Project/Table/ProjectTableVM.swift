//
//  ProjectTableVM.swift
//  Tanuki
//
//  Created by Lorenzo Lewis on 2/3/21.
//

import Combine

class ProjectTableVM: ObservableObject {
    
    private enum Section: Int, CaseIterable {
        case yourProjects
        case starredProjects
    }
        
    private var cancellable = Set<AnyCancellable>()
    
    @Published var didUpdateSection: Int?
    
    private var userProjects: [Project] = []
    private var starredProjects: [Project] = []
    
    init(userProjects: AnyPublisher<[Project], Error>, starredProjects: AnyPublisher<[Project], Error>) {
        userProjects
            .sink { completion in } receiveValue: { projects in
                self.userProjects = projects
                self.didUpdateSection = Section.yourProjects.rawValue
            }
            .store(in: &cancellable)
        starredProjects
            .sink { completion in } receiveValue: { projects in
                self.starredProjects = projects
                self.didUpdateSection = Section.starredProjects.rawValue
            }
            .store(in: &cancellable)
        
    }
    
    var numberOfSections: Int {
        Section.allCases.count
    }
    
    func rowsInSection(_ section: Int) -> Int {
        switch Section(rawValue: section) {
        case .yourProjects:
            return userProjects.count
        case .starredProjects:
            return starredProjects.count
        case .none:
            return 0
        }
    }
    
    func titleForSection(_ section: Int) -> String? {
        switch Section(rawValue: section) {
        case .yourProjects:
            return "Your Projects"
        case .starredProjects:
            return "Starred Projects"
        case .none:
            return nil
        }
    }
    
    func projectFor(section: Int, row: Int) -> Project? {
        switch Section(rawValue: section) {
        case .yourProjects:
            return userProjects[row]
        case .starredProjects:
            return starredProjects[row]
        case .none:
            return nil
        }
    }
}
