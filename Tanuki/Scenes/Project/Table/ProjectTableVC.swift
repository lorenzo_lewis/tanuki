//
//  HomeViewController.swift
//  Tanuki
//
//  Created by Lorenzo Lewis on 12/26/20.
//

import UIKit
import Combine

class ProjectTableVC: UITableViewController {
    
    weak var coordinator: MainCoordinator?
    private var viewModel: ProjectTableVM
    private var cancellable = Set<AnyCancellable>()
    
    init(viewModel: ProjectTableVM) {
        self.viewModel = viewModel
        super.init(style: .grouped)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        setupBindings()
    }
    
    private func setup() {
        view.backgroundColor = .systemBackground
        tableView.register(ProjectTableViewCell.self, forCellReuseIdentifier: "cell")
    }
    
    private func setupBindings() {
        viewModel.$didUpdateSection
            .receive(on: DispatchQueue.main)
            .sink { section in
                guard let section = section else { return }
                self.tableView.reloadSections(IndexSet(integer: section), with: .automatic)
            }
            .store(in: &cancellable)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        viewModel.numberOfSections
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.rowsInSection(section)
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        viewModel.titleForSection(section)
    }
        
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let project = viewModel.projectFor(section: indexPath.section, row: indexPath.row)
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! ProjectTableViewCell
        
        if let project = project {
            cell.configure(with: project)
        }
                    
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let project = viewModel.projectFor(section: indexPath.section, row: indexPath.row) else { return }
        
        coordinator?.showProject(project.id)
    }
}
