//
//  ProjectTableViewCell.swift
//  Tanuki
//
//  Created by Lorenzo Lewis on 2/4/21.
//

import UIKit

class ProjectTableViewCell: UITableViewCell {
    
    private var avatarView = GLAvatarImageView(size: .md)
    
    private var nameLabel: UILabel = {
        let view = UILabel()
        
        return view
    }()
    
    private var descriptionlabel: UILabel = {
        let view = UILabel()
        view.font = .preferredFont(forTextStyle: .caption1)
        
        view.isHidden = true
        
        return view
    }()
    
    private var labelStackView: UIStackView = {
        let view = UIStackView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.axis = .vertical
        
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(with project: Project) {
        avatarView.avatarImageUrl = project.avatarUrl
        nameLabel.text = project.name
        
        if let description = project.description {
            descriptionlabel.text = description
            descriptionlabel.isHidden = false
        }
        
        setup()
    }
    
    private func setup() {
        contentView.addSubviews(labelStackView, avatarView)
        labelStackView.addArrangedSubviews(nameLabel, descriptionlabel)
        
        NSLayoutConstraint.activate([
            avatarView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            avatarView.leadingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.leadingAnchor),
            
            labelStackView.topAnchor.constraint(equalTo: contentView.layoutMarginsGuide.topAnchor),
            labelStackView.leadingAnchor.constraint(equalTo: avatarView.trailingAnchor, constant: 8),
            labelStackView.bottomAnchor.constraint(equalTo: contentView.layoutMarginsGuide.bottomAnchor),
            labelStackView.trailingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.trailingAnchor)
        ])
    }
}
