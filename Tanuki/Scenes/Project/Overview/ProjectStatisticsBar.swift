//
//  ProjectStatisticsBar.swift
//  Tanuki
//
//  Created by Lorenzo Lewis on 1/25/21.
//

import UIKit
import Combine

class ProjectStatisticsBar: UIScrollView {

    private var viewModel: ProjectVM
    private var cancellable = Set<AnyCancellable>()

    private var stackView: UIStackView = {
        let view = UIStackView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.spacing = 16

        return view
    }()

    private lazy var starLabel: GLImageLabel = {
        let label = GLImageLabel()
        label.isHidden = true

        label.image.image = .glStar

        return label
    }()

    private lazy var forkLabel: GLImageLabel = {
        let label = GLImageLabel()
        label.isHidden = true

        label.image.image = .glFork

        return label
    }()

    private lazy var commitLabel: GLImageLabel = {
        let label = GLImageLabel()
        label.isHidden = true

        label.image.image = .glCommit

        return label
    }()

    private lazy var branchLabel: GLImageLabel = {
        let label = GLImageLabel()
        label.isHidden = true

        label.image.image = .glBranch

        return label
    }()

    private lazy var tagLabel: GLImageLabel = {
        let label = GLImageLabel()
        label.isHidden = true

        label.image.image = .glLabel

        return label
    }()

    private lazy var fileSizeLabel: GLImageLabel = {
        let label = GLImageLabel()
        label.isHidden = true

        label.image.image = .glDocCode

        return label
    }()

    private lazy var storageSizeLabel: GLImageLabel = {
        let label = GLImageLabel()
        label.isHidden = true

        label.image.image = .glDisk

        return label
    }()

    init(_ viewModel: ProjectVM) {
        self.viewModel = viewModel
        super.init(frame: .zero)
        configure()
        configureBindings()
    }

    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func configure() {
        self.addSubview(stackView)
        self.showsHorizontalScrollIndicator = false
        self.translatesAutoresizingMaskIntoConstraints = false

        stackView.addArrangedSubviews(starLabel, forkLabel, commitLabel, branchLabel, tagLabel, fileSizeLabel, storageSizeLabel)

        NSLayoutConstraint.activate([
            stackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor),
            heightAnchor.constraint(equalTo: stackView.heightAnchor)
        ])
    }

    private func configureBindings() {
        viewModel.$starLabel
            .sink(receiveValue: { string in
                self.starLabel.label.text = string
                self.starLabel.isHidden = false
            })
            .store(in: &cancellable)
        viewModel.$forkLabel
            .sink(receiveValue: { string in
                self.forkLabel.label.text = string
                self.forkLabel.isHidden = false
            })
            .store(in: &cancellable)
//        viewModel.$commitLabel
//            .sink(receiveValue: { string in
//                self.commitLabel.label.text = string
//                self.commitLabel.isHidden = false
//            })
//            .store(in: &cancellable)
//        viewModel.$branchLabel
//            .sink(receiveValue: { string in
//                self.branchLabel.label.text = string
//                self.branchLabel.isHidden = false
//            })
//            .store(in: &cancellable)
        viewModel.$tagLabel
            .sink(receiveValue: { string in
                self.tagLabel.label.text = string
                self.tagLabel.isHidden = false
            })
            .store(in: &cancellable)
//        viewModel.$fileSizeLabel
//            .sink(receiveValue: { string in
//                self.fileSizeLabel.label.text = string
//                self.fileSizeLabel.isHidden = false
//            })
//            .store(in: &cancellable)
//        viewModel.$storageSizeLabel
//            .sink(receiveValue: { string in
//                self.storageSizeLabel.label.text = string
//                self.storageSizeLabel.isHidden = false
//            })
//            .store(in: &cancellable)
    }
}
