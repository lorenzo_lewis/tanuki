//
//  ProjectOverviewViewController.swift
//  Tanuki
//
//  Created by Lorenzo Lewis on 1/11/21.
//

import UIKit
import Combine

class ProjectDetailsVC: UIViewController {
    
    // MARK:- Views
    private var scrollView: UIScrollView = {
        let view = UIScrollView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.alwaysBounceVertical = true

        return view
    }()
    
    private var stackView: UIStackView = {
        let view = UIStackView()
        
        view.axis = .vertical
        view.spacing = 16
        
        return view
    }()
    
    // MARK: - Header
    private lazy var headerView: UIStackView = {
        let view = UIStackView()
        view.spacing = 8
        view.alignment = .center
        view.addArrangedSubviews(avatarImageView, labelStackView)
        
        return view
    }()
    
    private var avatarImageView: GLAvatarImageView = {
        let view = GLAvatarImageView(size: .xl)
        
        return view
    }()
    
    private lazy var labelStackView: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        view.addArrangedSubviews(namespaceLabel, titleLabel)
        
        return view
    }()
    
    private var namespaceLabel: UILabel = {
        let label = UILabel()
        label.font = .preferredFont(forTextStyle: .caption1)
        
        return label
    }()
    
    private var titleLabel: UILabel = {
        let label = UILabel()
        label.font = .preferredFont(forTextStyle: .title2)
        
        return label
    }()
    
    private var descriptionLabel: UILabel = {
        let view = UILabel()
        view.numberOfLines = 3
        
        return view
    }()
    
    private lazy var statisticsBar: ProjectStatisticsBar = {
        let view = ProjectStatisticsBar(viewModel)
        
        return view
    }()
    
    private var repoTreeView: RepoTreeView = {
        let view = RepoTreeView()
        
        return view
    }()
        
    // MARK:- Variables
    weak var coordinator: MainCoordinator?
    private var viewModel: ProjectVM
    private var cancellable = Set<AnyCancellable>()
    
    // MARK:- Init
    init(_ viewModel: ProjectVM) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        setupBindings()
    }
    
    
    private func setup() {
        view.backgroundColor = .systemBackground

        view.addSubview(scrollView)
        scrollView.addSubview(stackView)
        scrollView.constraintToParentLayoutMarginsGuide()
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            
            stackView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            stackView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            stackView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            stackView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            stackView.widthAnchor.constraint(equalTo: scrollView.widthAnchor)
        ])
        
        stackView.addArrangedSubviews(headerView, descriptionLabel, statisticsBar, repoTreeView)
    }
    
    private func setupBindings() {
        viewModel.$avatarImageUrl.assign(to: \.avatarImageUrl, on: avatarImageView).store(in: &cancellable)
        viewModel.$projectNamespace.assign(to: \.text, on: namespaceLabel).store(in: &cancellable)
        viewModel.$projectTitle.assign(to: \.text, on: titleLabel).store(in: &cancellable)
        
        viewModel.$description.assign(to: \.text, on: descriptionLabel).store(in: &cancellable)
        
        viewModel.$repoTreeFiles.assign(to: \.files, on: repoTreeView).store(in: &cancellable)
    }
}

#if DEBUG
import SwiftUI
struct ProjectOverviewPreview: PreviewProvider {
    
    static let project = Just(Project.mock).setFailureType(to: Error.self).eraseToAnyPublisher()
    static let vm = ProjectVM(project: project)
    
    static var previews: some View {
        UIViewControllerContainerView(viewController: ProjectDetailsVC(vm))
            .preferredColorScheme(.dark)
    }
}
#endif
