//
//  ProjectOverviewViewModel.swift
//  Tanuki
//
//  Created by Lorenzo Lewis on 1/26/21.
//

import UIKit
import Combine

class ProjectVM: ObservableObject {
    
    @Published var avatarImageUrl: String?
    @Published var projectNamespace: String?
    @Published var projectTitle: String?
    @Published var description: String?
    
    // Statistics
    @Published var starLabel: String?
    @Published var forkLabel: String?
    @Published var commitLabel: String?
    @Published var branchLabel: String?
    @Published var tagLabel: String?
    @Published var fileSizeLabel: String?
    @Published var storageSizeLabel: String?
    
    // Repo tree
    @Published var repoTreeFiles: [RepoTreeNode] = []
    
    private var cancellable = Set<AnyCancellable>()
    
//    private let byteCountFormatter: ByteCountFormatter = {
//        let formatter = ByteCountFormatter()
//
//        return formatter
//    }()
    
    init(project: AnyPublisher<Project, Error>) {
        project
            .flatMap { project in
                Network().requestDecoded([RepoTreeNode].self, from: Endpoint.Project.listRepoTree(project: project.id)).mapError { $0 as Error }
                    .map { nodes in (project: project, nodes: nodes) }
            }
            .receive(on: DispatchQueue.main)
            .sink { _ in } receiveValue: { result in
                self.avatarImageUrl = result.project.avatarUrl
                self.projectNamespace = result.project.namespace.name
                self.projectTitle = result.project.name
                self.description = result.project.description

                // Statistics
                self.starLabel = "\(result.project.starCount) Stars"
                self.forkLabel = "\(result.project.forksCount) Forks"
                //            self.commitLabel = "\(project.statistics?.commitCount) Commits"
                self.tagLabel = "\(result.project.tagList.count) Tags"
                //            if let fileSize = self.byteCountFormatter.string(for: project.statistics?.repositorySize) {
                //                self.fileSizeLabel = "\(fileSize) Files"
                //            }
                //
                //            if let storageSize = self.byteCountFormatter.string(for: project.statistics?.storageSize) {
                //                self.storageSizeLabel = "\(storageSize) Storage"
                //            }
                
                // Repo tree
                self.repoTreeFiles = result.nodes
            }
            .store(in: &cancellable)
//
//        //        branches
//        //            .receive(on: DispatchQueue.main)
//        //            .sink { _ in } receiveValue: { branches in
//        //            self.branchLabel = "\(branches.count) Branches"
//        //        }
//        //        .store(in: &cancellable)
    }
}
