//
//  RepoTreeRowView.swift
//  Tanuki
//
//  Created by Lorenzo Lewis on 2/9/21.
//

import UIKit

class RepoTreeRowView: UIStackView {

    var nameLabel: UILabel = {
        let view = UILabel()
        
        return view
    }()
    
    var updateLabel: UILabel = {
        let view = UILabel()
        
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        addArrangedSubviews(nameLabel, updateLabel)
        isLayoutMarginsRelativeArrangement = true
        directionalLayoutMargins = .init(top: 8, leading: 8, bottom: 8, trailing: 8)
        layer.borderWidth = 1
        layer.borderColor = UIColor.tertiarySystemBackground.cgColor
    }
}

#if DEBUG
import SwiftUI
struct RepoTreeRowPreview: PreviewProvider {
    
    static var previews: some View {
        UIViewContainerView(view: RepoTreeRowView())
            .padding()
            .previewLayout(.fixed(width: 300, height: 200))
    }
}
#endif
