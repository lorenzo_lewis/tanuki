//
//  RepoTreeHeaderView.swift
//  Tanuki
//
//  Created by Lorenzo Lewis on 2/9/21.
//

import UIKit

class RepoTreeHeaderView: RepoTreeRowView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        nameLabel.text = "Name"
        backgroundColor = UIColor.secondarySystemBackground
        layer.cornerRadius = 4
        layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }
    
}

#if DEBUG
import SwiftUI
struct TreeHeaderPreview: PreviewProvider {
    
    static var previews: some View {
        UIViewContainerView(view: RepoTreeHeaderView())
    }
}
#endif
