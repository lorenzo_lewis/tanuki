//
//  RepoTreeView.swift
//  Tanuki
//
//  Created by Lorenzo Lewis on 2/13/21.
//

import UIKit

class RepoTreeView: UIStackView {
    
    var files: [RepoTreeNode] = [] {
        didSet {
            for (index, file) in files.enumerated() {
                let view = RepoTreeRowView()
                view.nameLabel.text = file.name
                
                if index == files.count {
                    view.layer.cornerRadius = 4
                    view.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
                }
                
                addArrangedSubview(view)
            }
        }
    }
    
    private var headerView: RepoTreeHeaderView = {
        let view = RepoTreeHeaderView()
        
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        axis = .vertical
        addArrangedSubview(headerView)
        //        layer.cornerRadius = 4
        //        layer.borderWidth = 1
        //        layer.borderColor = UIColor.systemBlue.cgColor
    }
}

#if DEBUG
import SwiftUI
struct RepoTreePreview: PreviewProvider {
    
    static var repoTreeView: RepoTreeView = {
        let view = RepoTreeView()
        view.files = [RepoTreeNode.mock]
        
        return view
    }()
    
    static var previews: some View {
        UIViewContainerView(view: repoTreeView)
            .padding()
            .previewLayout(.fixed(width: 300, height: 200))
    }
}
#endif
