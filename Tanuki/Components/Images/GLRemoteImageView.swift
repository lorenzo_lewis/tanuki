//
//  GLRemoteImageView.swift
//  Tanuki
//
//  Created by Lorenzo Lewis on 1/12/21.
//

import UIKit
import Combine

class GLRemoteImageView: UIImageView {

    private var cancellable: AnyCancellable?
    var imageUrl: String? {
        didSet { loadImage(from: imageUrl) }
    }
    
    init() {
        super.init(frame: .zero)
        self.translatesAutoresizingMaskIntoConstraints = false
    }
    
    private func loadImage(from urlString: String?) {
        guard let string = urlString, let url = URL(string: string) else { return }

        cancellable = Network().requestData(from: URLRequest(url: url))
            .map { data in
                if let image = UIImage(data: data) {
//                    onImageLoaded()
                    return image
                }
                return nil
            }
            .replaceError(with: nil)
            .receive(on: DispatchQueue.main)
            .assign(to: \.image, on: self)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
