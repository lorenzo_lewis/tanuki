//
//  AvatarImageView.swift
//  Tanuki
//
//  Created by Lorenzo Lewis on 2/7/21.
//

import UIKit

class GLAvatarImageView: UIImageView {
    
    internal enum Size: CGFloat {
        case xs = 16
        case sm = 24
        case md = 32
        case lg = 48
        case xl = 64
        case xxl = 96
    }
    
    internal enum AvatarType {
        case project
        case user
    }
    
    private let backgroundColors: [UIColor] = [.glBlue4, .glGreen4, .glOrange4, .glRed4, .glPurple4]
    
    private var placeholderLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textAlignment = .center
        
        return view
    }()
    
    private let size: Size
    private let type: AvatarType
    
    var avatarImageUrl: String? {
        didSet {
            setupRemoteImage(with: avatarImageUrl)
        }
    }
    
    init(size: Size) {
        self.size = size
        self.type = .project
        super.init(frame: .zero)
        
        setup()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var intrinsicContentSize: CGSize {
        CGSize(width: size.rawValue, height: size.rawValue)
    }
    
    // MARK:- Functions
    
    private func setup() {
        self.translatesAutoresizingMaskIntoConstraints = false
        widthAnchor.constraint(equalToConstant: size.rawValue).isActive = true
        heightAnchor.constraint(equalToConstant: size.rawValue).isActive = true
        // https://useyourloaf.com/blog/xcode-previews-for-view-controllers/
        setContentHuggingPriority(.defaultHigh, for: .vertical)
        setContentHuggingPriority(.defaultHigh, for: .horizontal)
                
        layer.cornerRadius = type == .user ? size.rawValue / 2 : 4
        layer.masksToBounds = true
        
        layer.borderWidth = 1
        layer.borderColor = UIColor.glGray6.cgColor
    }
    
    private func setupPlaceholderView(with string: String, id: Int) {
        addSubview(placeholderLabel)
        placeholderLabel.constraintToParent()
        
        placeholderLabel.backgroundColor = backgroundColors[id % backgroundColors.count]
        
        guard let letter = string.first else { return }
        placeholderLabel.text = String(letter).uppercased()
    }
    
    private func setupRemoteImage(with string: String?) {
        let remoteImage = GLRemoteImageView()
        
        remoteImage.imageUrl = string
        
        addSubview(remoteImage)
        remoteImage.constraintToParent()
    }
}

#if DEBUG
import SwiftUI

struct AvatarImagePreview: PreviewProvider {
    static let project = Project.mock
    
    static var previews: some View {
        Group {
//            UIViewContainerView(view: AvatarImageView(avatarUrl: project.avatarUrl, size: .xs, type: .project))
//            UIViewContainerView(view: AvatarImageView(avatarUrl: project.avatarUrl, size: .sm, type: .project))
//            UIViewContainerView(view: AvatarImageView(avatarUrl: project.avatarUrl, size: .md, type: .project))
//            UIViewContainerView(view: AvatarImageView(avatarUrl: project.avatarUrl, size: .lg, type: .project))
            UIViewContainerView(view: GLAvatarImageView(size: .xl))
            UIViewContainerView(view: GLAvatarImageView(size: .xxl))
        }
        .padding(5)
        .previewLayout(.sizeThatFits)
    }
}
#endif

