//
//  GLImageLabel.swift
//  Tanuki
//
//  Created by Lorenzo Lewis on 1/16/21.
//

import UIKit

class GLImageLabel: UIView {

    let image: UIImageView = {
        let image = UIImageView()

        image.translatesAutoresizingMaskIntoConstraints = false

        return image
    }()

    let label: UILabel = {
        let label = UILabel()

        label.translatesAutoresizingMaskIntoConstraints = false

        return label
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }

    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func configure() {
        self.translatesAutoresizingMaskIntoConstraints = false

        addSubview(image)
        addSubview(label)

        NSLayoutConstraint.activate([
            image.leadingAnchor.constraint(equalTo: leadingAnchor),
            image.centerYAnchor.constraint(equalTo: centerYAnchor),
            label.leadingAnchor.constraint(equalTo: image.trailingAnchor, constant: 4),
            label.trailingAnchor.constraint(equalTo: trailingAnchor)
        ])
    }

    override var intrinsicContentSize: CGSize {

        let width = label.frame.maxX - image.frame.minX
        let height = max(image.intrinsicContentSize.height, label.intrinsicContentSize.height)

        return CGSize(width: width, height: height)
    }
}
