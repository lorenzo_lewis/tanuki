//
//  UIViewPreviewProvider.swift
//  Tanuki
//
//  Created by Lorenzo Lewis on 1/12/21.
//

import SwiftUI

struct UIViewControllerContainerView: UIViewControllerRepresentable {

    let viewController: UIViewController

    func makeUIViewController(context: Context) -> some UIViewController {
        return viewController
    }

    func updateUIViewController(_ uiViewController: UIViewControllerType, context: Context) {}
}

struct UIViewContainerView: UIViewRepresentable {

    let view: UIView

    func makeUIView(context: Context) -> some UIView {
        return view
    }

    func updateUIView(_ uiView: UIViewType, context: Context) {}
}
