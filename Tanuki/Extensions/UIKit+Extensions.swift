//
//  UIKit+Extensions.swift
//  Tanuki
//
//  Created by Lorenzo Lewis on 1/26/21.
//

import UIKit

extension UIStackView {
    func addArrangedSubviews(_ views: UIView...) {
        views.forEach { addArrangedSubview($0) }
    }
}

extension UIView {
    func addSubviews(_ views: UIView...) {
        views.forEach { addSubview($0) }
    }
    
    func constraintToParent(padding: CGFloat = 0) {
        guard let superview = superview else { return }
        self.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            self.topAnchor.constraint(equalTo: superview.topAnchor, constant: padding),
            self.leadingAnchor.constraint(equalTo: superview.leadingAnchor, constant: padding),
            self.bottomAnchor.constraint(equalTo: superview.bottomAnchor, constant: -padding),
            self.trailingAnchor.constraint(equalTo: superview.trailingAnchor, constant: -padding)
        ])
    }
    
    func constraintToParentLayoutMarginsGuide(padding: CGFloat = 0) {
        guard let superview = superview else { return }
        self.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            self.topAnchor.constraint(equalTo: superview.layoutMarginsGuide.topAnchor, constant: padding),
            self.leadingAnchor.constraint(equalTo: superview.layoutMarginsGuide.leadingAnchor, constant: padding),
            self.bottomAnchor.constraint(equalTo: superview.layoutMarginsGuide.bottomAnchor, constant: -padding),
            self.trailingAnchor.constraint(equalTo: superview.layoutMarginsGuide.trailingAnchor, constant: -padding)
        ])
    }
}
