//
//  MainCoordinator.swift
//  Tanuki
//
//  Created by Lorenzo Lewis on 1/25/21.
//

import UIKit
import Combine

protocol Coordinator: class {
    var navigationController: UINavigationController { get set }

    func start()
}

class MainCoordinator: Coordinator {
    var navigationController = UINavigationController()
    
    func start() {        
        AuthManager.shared.setEmbedded(navigationController)
        showProjectList()
    }
    
    func showProjectList() {
        let worker = ProjectWorker()
        
        let userProjects = worker.currentUsersProjects()
        let starredProjects = worker.currentUsersStarredProjects()
        let vm = ProjectTableVM(userProjects: userProjects, starredProjects: starredProjects)
        let vc = ProjectTableVC(viewModel: vm)
        vc.coordinator = self
        
        navigationController.pushViewController(vc, animated: false)
    }
    
    func showProject(_ project: Int) {
        let worker = ProjectWorker()
                
        let project = worker.project(project)
        let vm = ProjectVM(project: project)
        let vc = ProjectDetailsVC(vm)
        vc.coordinator = self
        
        navigationController.pushViewController(vc, animated: true)
    }
}
