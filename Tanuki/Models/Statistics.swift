//
//  Statistics.swift
//  Tanuki
//
//  Created by Lorenzo Lewis on 1/16/21.
//

import Foundation

struct Statistics: Codable {
    let commitCount: Int
    let storageSize: Int
    let repositorySize: Int
}

extension Statistics: MockModel {
    static let mock = Statistics(
        commitCount: 3,
        storageSize: 4,
        repositorySize: 23
    )
}
