//
//  Namespace.swift
//  Tanuki
//
//  Created by Lorenzo Lewis on 12/27/20.
//

import Foundation

struct Namespace: Codable {
    let id: Int
    let name: String
}

extension Namespace: MockModel {
    static let mock = Namespace(
        id: 10312934,
        name: "Lorenzo Lewis"
    )
}
