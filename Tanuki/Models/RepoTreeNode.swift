//
//  RepoTreeNode.swift
//  Tanuki
//
//  Created by Lorenzo Lewis on 2/10/21.
//

import Foundation

struct RepoTreeNode: Codable {
    let id: String
    let name: String
    let type: String
    let path: String
    let mode: String
}

extension RepoTreeNode: MockModel {
    static var mock: RepoTreeNode {
        RepoTreeNode(
            id: "a1e8f8d745cc87e3a9248358d9352bb7f9a0aeba",
            name: "html",
            type: "tree",
            path: "files/html",
            mode: "040000"
        )
    }
}
