//
//  ModelProtocol.swift
//  Tanuki
//
//  Created by Lorenzo Lewis on 2/6/21.
//

import Foundation

protocol MockModel {
    static var mock: Self { get }
}
