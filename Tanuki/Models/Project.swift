//
//  Project.swift
//  Tanuki
//
//  Created by Lorenzo Lewis on 12/27/20.
//

import Foundation

struct Project: Codable {
    let id: Int
    let description: String?
    let name: String
    var avatarUrl: String?
    let forksCount: Int
    let starCount: Int
    let lastActivityAt: String
    let namespace: Namespace
    let statistics: Statistics?
    let tagList: [String]
}

extension Project: MockModel {
    
    static let mock = Project(
        id: 22901230,
        description: "I'm a cool little description that goes on and on and on. I probably wrap onto the next line so that should be a good test of that.",
        name: "TimerLoop",
        avatarUrl: "https://assets.gitlab-static.net/uploads/-/system/project/avatar/22901230/Icon.png",
        forksCount: 2,
        starCount: 4,
        lastActivityAt: "654654654654654",
        namespace: Namespace.mock,
        statistics: Statistics.mock,
        tagList: ["Tag1", "Tag2"]
    )
}
