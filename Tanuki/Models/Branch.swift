//
//  Branch.swift
//  Tanuki
//
//  Created by Lorenzo Lewis on 1/25/21.
//

import Foundation

typealias Branches = [Branch]

struct Branch: Codable { }
