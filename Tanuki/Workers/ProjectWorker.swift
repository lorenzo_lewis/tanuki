//
//  ProjectWorker.swift
//  Tanuki
//
//  Created by Lorenzo Lewis on 2/6/21.
//

import Foundation
import Combine

struct ProjectWorker {
    
    private let network = Network()
    
    func currentUsersProjects() -> AnyPublisher<[Project], Error> {
        network.requestDecoded(User.self, from: Endpoint.User.currentUser())
            .flatMap { user in
                network.requestDecoded([Project].self, from: Endpoint.Project.getProjectsFor(user: user.id))
            }
            .mapError { $0 as Error }
            .eraseToAnyPublisher()
    }
    
    func currentUsersStarredProjects() -> AnyPublisher<[Project], Error> {
        network.requestDecoded(User.self, from: Endpoint.User.currentUser())
            .flatMap { user in
                network.requestDecoded([Project].self, from: Endpoint.Project.getStarredProjectsFor(user: user.id))
            }
            .mapError { $0 as Error }
            .eraseToAnyPublisher()
    }
    
    func project(_ project: Int) -> AnyPublisher<Project, Error> {
        network.requestDecoded(Project.self, from: Endpoint.Project.project(project))
            .mapError { $0 as Error }
            .eraseToAnyPublisher()
    }
}
