//
//  ProjectEndpoint.swift
//  Tanuki
//
//  Created by Lorenzo Lewis on 1/25/21.
//

import Foundation

extension Endpoint {
    
    enum Project {
        static func project(_ id: Int) -> URLRequest {
            Endpoint(
                path: "projects/\(id)",
                queryItems: [URLQueryItem(name: "statistics", value: "true")]
            ).request
        }
        
        static func getProjectsFor(user: Int) -> URLRequest {
            Endpoint(
                path: "users/\(user)/projects",
                queryItems: [URLQueryItem(name: "statistics", value: "true")]
            ).request
        }
        
        static func getStarredProjectsFor(user: Int) -> URLRequest {
            Endpoint(
                path: "users/\(user)/starred_projects",
                queryItems: [URLQueryItem(name: "statistics", value: "true")]
            ).request
        }
        
        static func listRepoTree(project: Int) -> URLRequest {
            Endpoint(
                path: "projects/\(project)/repository/tree"
            ).request
        }
    }
}
