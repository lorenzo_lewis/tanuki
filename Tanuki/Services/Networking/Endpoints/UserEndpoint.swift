//
//  UserEndpoint.swift
//  Tanuki
//
//  Created by Lorenzo Lewis on 2/6/21.
//

import Foundation

extension Endpoint {
    enum User {        
        static func currentUser() -> URLRequest {
            Endpoint(path: "user").request
        }
    }
}
