//
//  Endpoint.swift
//  Tanuki
//
//  Created by Lorenzo Lewis on 1/25/21.
//

import Foundation

struct Endpoint {
    internal let path: String
    internal var queryItems: [URLQueryItem] = []
    
    internal var request: URLRequest {
        var componenets = URLComponents()
        componenets.scheme = "https"
        componenets.host = "gitlab.com"
        componenets.path = "/api/v4/" + path
        componenets.queryItems = queryItems
        
        guard let url = componenets.url else {
            preconditionFailure("URL could not be genereated from components \(componenets)")
        }
        
        return URLRequest(url: url)
    }
}
