//
//  NetworkProtocol.swift
//  Tanuki
//
//  Created by Lorenzo Lewis on 2/6/21.
//

import Foundation
import Combine
import OAuth2

protocol NetworkProtocol {
    func requestDecoded<T: Decodable>(_: T.Type, from request: URLRequest) -> AnyPublisher<T, NetworkError>
    func requestData(from request: URLRequest) -> AnyPublisher<Data, NetworkError>
}

enum NetworkError: Error {
    case sessionError
    case notAuthenticated
    case decodingFailed
    case invalidUrl
    case noDataReturned
    case unknownError(Error)
}

class Network: NetworkProtocol {
    
    private let auth = AuthManager.shared.auth
    private lazy var loader = OAuth2DataLoader(oauth2: auth)
    
    private var decoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        return decoder
    }()
        
    func requestDecoded<T: Decodable>(_: T.Type, from request: URLRequest) -> AnyPublisher<T, NetworkError> {
        guard let url = request.url else {
            return Fail(error: NetworkError.invalidUrl).eraseToAnyPublisher()
        }
        
        let request = auth.request(forURL: url)
        
        return requestData(from: request)
            .tryMap { data in
                do {
                    let decodedData = try self.decoder.decode(T.self, from: data)
                    return decodedData
                } catch {
                    log.error("Issue occured while attempting to decode: \(error)")
                    log.debug("Data returned: \(String(describing: String(data: data, encoding: .utf8)))")
                    throw NetworkError.decodingFailed
                }
            }
            .mapError { error -> NetworkError in
                return error as? NetworkError ?? .unknownError(error)
            }
            .eraseToAnyPublisher()
    }
    
    func requestData(from request: URLRequest) -> AnyPublisher<Data, NetworkError> {
        guard let url = request.url else {
            return Fail(error: NetworkError.invalidUrl).eraseToAnyPublisher()
        }
        
        let request = auth.request(forURL: url)
        
        return Future<Data, NetworkError> { promise in
            self.loader.perform(request: request) { response in
                if let error = response.error {
                    promise(.failure(.unknownError(error)))
                    return
                }
                
                guard let data = response.data else {
                    promise(.failure(.noDataReturned))
                    return
                }
                
                promise(.success(data))
            }
        }
        .eraseToAnyPublisher()
    }
}
