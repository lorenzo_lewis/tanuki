//
//  CloudKitManager.swift
//  Tanuki
//
//  Created by Lorenzo Lewis on 1/17/21.
//

import CloudKit
import Combine

struct CloudKitManager {

    let container = CKContainer(identifier: "iCloud.click.lorenzolewis.tanuki")
    let recordType = "Host"

    struct CloudKitHost {
        var authorizationPath: String!
        var clientId: String!
        var hostPath: String!
        var redirectUri: String!
        var scope: String!
        var tokenPath: String!
    }

    enum CloudKitManagerError: Error {
        case noRecordsReturned
    }

    func getHost(name: String) -> AnyPublisher<CloudKitHost, CloudKitManagerError> {
        let predicate = NSPredicate(format: "hostPath == %@", name)
        let query = CKQuery(recordType: recordType, predicate: predicate)

        return Future<CloudKitHost, CloudKitManagerError> { promise in

            container.publicCloudDatabase.perform(query, inZoneWith: nil) { records, error in
                guard let records = records else {
                    promise(.failure(.noRecordsReturned))
                    return
                }

                let hosts = records.map { record -> CloudKitHost in

                    var host = CloudKitHost()
                    host.authorizationPath = record["authorizationPath"]
                    host.clientId = record["clientId"]
                    host.hostPath = record["hostPath"]
                    host.redirectUri = record["redirectUri"]
                    host.scope = record["scope"]
                    host.tokenPath = record ["tokenPath"]

                    return host
                }

                guard let host = hosts.first else {
                    promise(.failure(.noRecordsReturned))
                    return
                }

                promise(.success(host))
            }
        }
        .eraseToAnyPublisher()
    }
}
