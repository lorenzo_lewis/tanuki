//
//  AuthManager.swift
//  Tanuki
//
//  Created by Lorenzo Lewis on 2/6/21.
//

import Foundation
import OAuth2
import Combine

class AuthManager {
    
    static let shared = AuthManager()
    
    private(set) var auth: OAuth2
        
    private init() {
        auth = OAuth2CodeGrant(settings: [
            "client_id": "6104c1097752383ba6b35fbf6f0f476d7214a85f11ebfed6504903398f0dda83",
            "authorize_uri": "https://gitlab.com/oauth/authorize",
            "token_uri": "https://gitlab.com/oauth/token",
            "redirect_uris": ["tanuki://authorize"],
            "scope": "read_api"
            
        ] as OAuth2JSON)
    }
    
    func setEmbedded(_ object: AnyObject) {
        auth.authConfig.authorizeEmbedded = true
        auth.authConfig.authorizeContext = object
    }
}
